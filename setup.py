#!/usr/bin/env python
from setuptools import setup

setup(
    name="btrfs_maintenance",
    version="1.0.2",
    author="5ive",
    description="Maintains btrfs volumes",
    packages=["btrfs_maintenance"],
    install_requires=[],
    entry_points={
        "console_scripts": ["btrfs-balance=btrfs_maintenance.btrfs_balance:main"]
    },
)
