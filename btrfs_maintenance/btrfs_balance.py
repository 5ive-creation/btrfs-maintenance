#!/usr/bin/python3

import subprocess
import argparse
from re import finditer
import re


def get_btrfs_mounts():
    regex = r"(/dev/disk/by-uuid/|/dev/|UUID=)([\S]+)[\s]+(/([\S]+|)).*btrfs"
    with open("/etc/fstab", "r") as f:
        return finditer(regex, f.read(), re.MULTILINE)


def main():
    parser = argparse.ArgumentParser(
        description="Balance btrfs volumes",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "-v", "--verbose", help="more verbose output to console", action="store_true"
    )
    parser.add_argument(
        "-d",
        metavar="[filter]",
        dest="data",
        help="act on data chunks (default: usage=10)",
        default="usage=10",
    )
    parser.add_argument(
        "-m",
        metavar="[filter]",
        dest="metadata",
        help="act on metadata chunks (default: usage=10)",
        default="usage=10",
    )
    parser.add_argument(
        "-n",
        "--dry-run",
        help="make no changes to volumes. Simply print out what would be done",
        action="store_true",
    )
    parser.add_argument(
        "-f",
        "--full",
        help="WARNING: this is a long intensive process depending on size of your volume",
        action="store_true",
    )
    args = parser.parse_args()

    balanced = []
    for mount in get_btrfs_mounts():
        if not mount.group(2) in balanced:
            if args.verbose:
                print("INFO: {} will be balanced".format(mount.group(2)))
            balance_command = ["btrfs", "balance", "start"]
            if args.full:
                balance_command = balance_command + ["--full-balance"]
            if not args.full:
                balance_command.extend(["-d" + args.data, "-m" + args.metadata])
            if args.verbose:
                balance_command.append("-v")
            balance_command.append(mount.group(3).strip())
            if args.verbose or args.dry_run:
                print("INFO: " + " ".join(balance_command))
            if not args.dry_run:
                subprocess.run(balance_command)
            else:
                print("INFO: no commands have been executed")
            balanced.append(mount.group(2))


if __name__ == "__main__":
    main()
